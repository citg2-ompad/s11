package com.example.discussion.controller;

import com.example.discussion.models.User;
import com.example.discussion.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController

@CrossOrigin
public class UserController {
    @Autowired
    UserService userService;
    // Create a user
    @RequestMapping(value ="/users",method= RequestMethod.POST)
    public ResponseEntity<Object> createUser(@RequestBody User user){
        userService.createUser(user);
        return new ResponseEntity<>("User created successfully!", HttpStatus.CREATED);
    }
    // Get all Users
    @RequestMapping(value="/users",method=RequestMethod.GET)
    public ResponseEntity<Object> getUsers(){
        return new ResponseEntity<>(userService.getUser(),HttpStatus.OK);
    }
    //  Delete a User
    @RequestMapping(value="/users/{userid}",method=RequestMethod.DELETE)
    public ResponseEntity<Object> deleteUser(@PathVariable Long userid){
        return userService.deleteUser(userid);
    }
    // Update a User
    @RequestMapping(value="/users/{userid}",method=RequestMethod.PUT)
    public ResponseEntity<Object> updateUser(@PathVariable Long userid,@RequestBody User user){
        return userService.updateUser(userid,user);
    }
}
