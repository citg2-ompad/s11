package com.example.discussion.services;
import com.example.discussion.repositories.PostRepository;
import com.example.discussion.models.Post;
import org.springframework.http.ResponseEntity;



public interface PostService {

    void createPost(Post post);

    //We can iterate/loop over the posts in order to display them
    Iterable<Post> getPost();

    ResponseEntity deletePost(Long id);
    ResponseEntity updatePost(Long id, Post post);
}